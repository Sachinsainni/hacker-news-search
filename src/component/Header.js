import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <>
        <div id="header">
          <div id="search-logo">
            <div id="logo-img">
              <img
                src="https://hn.algolia.com/packs/media/images/logo-hn-search-a822432b.png"
                alt="logo"
              />
            </div>
            <div id="logo-name">
              Search
              <br />
              Hacker News
            </div>
          </div>
          <div id="search-bar">
            <input
              type="search"
              name=""
              id="search-input"
              placeholder="Search stories by title, URl or author"
            />
            <div id="last-search-bar">
              <span >Search by</span>
              <img
                src="https://hn.algolia.com/packs/media/images/logo-algolia-blue-35c461b6.svg"
                alt=""
              />
            </div>
          </div>
          <div id="settings">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg>
            <span className="settings-name"> Settings</span>
          </div>
        </div>
      </>
    );
  }
}
