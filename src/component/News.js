import "./News.css";

const News = (props) => {
  const { newsDetails } = props;
  const { data } = newsDetails;
  const { title } = data;
  const { url } = data;
  const { score } = data;
  const { by } = data;
  const time = parseInt((newsDetails.data.time / 3.154e13) * 100000);
  const { descendants } = data ; 

  if (newsDetails.data.type === "story") {
    // console.log(newsDetails.data);
    return (
      <>
        <div className="news-container">
          <div className="parent-firstline">
            <span className="news-text">{title} </span>
            <a href={url} className="news-url">
              ({url}){" "}
            </a>
          </div>
          <div className="news-details">
            <span>{score} points</span>|<span>{by} </span> |{" "}
            <span>{time} year ago</span> | <span> {descendants} comments</span>
          </div>
        </div>
      </>
    );
  }
};

export default News;
