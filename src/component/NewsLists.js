import React, { Component } from "react";
import News from "./News";
import Axios from "axios";

class NewsLists extends Component {
  constructor() {
    super();

    this.APP_STATE = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
    };

    this.state = {
      status: this.APP_STATE.LOADING,
      newsArray: [],
      searchInput : "",
    };
  }

  beststories = () => {
    Axios.get(
      `https://hacker-news.firebaseio.com/v0/beststories.json?print=pretty`
    )
      .then((response) => {
        this.getNews(response.data);
        this.setState({
          status: this.APP_STATE.LOADED
        })
        return response;
      })
      .catch((err) => {
        console.log(err);
        this.setState({
          status: this.APP_STATE.ERROR,
        });
      });
  };
  getNews = (data) => {
    data.map((item) => {
       Axios.get(
        `https://hacker-news.firebaseio.com/v0/item/${item}.json?print=pretty`
      )
        .then((response) => {
          this.setState((prevState) => (
            {
            newsArray: [...prevState.newsArray, response],
          }
          ));
        })
        .catch((error) => {
          console.log(error);
          this.setState({
            status: this.APP_STATE.ERROR,
          });
        });
    });
    // }
  };

  onChangeSearchInput=(event)=>{
    this.setState({searchInput:event.target.value})

  }
  componentDidMount() {
    this.beststories();
  }

  render() {
    const { newsArray,searchInput } = this.state;

    const filteredNewsArray = newsArray.filter(news => news.data.title.toLowerCase().includes((searchInput.toLowerCase())))
    return (
      <>
      <div id="header">
          <div id="search-logo">
            <div id="logo-img">
              <img
                src="https://hn.algolia.com/packs/media/images/logo-hn-search-a822432b.png"
                alt="logo"
              />
            </div>
            <div id="logo-name">
              Search
              <br />
              Hacker News
            </div>
          </div>
          <div id="search-bar">
            <input
              type="search"
              id="search-input"
              onChange={this.onChangeSearchInput}
              value = {searchInput}
              placeholder="Search stories by title, URl or author"
            />
            <div id="last-search-bar">
              <span >Search by</span>
              <img
                src="https://hn.algolia.com/packs/media/images/logo-algolia-blue-35c461b6.svg"
                alt=""
              />
            </div>
          </div>
          <div id="settings">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg>
            <span className="settings-name"> Settings</span>
          </div>
        </div>
        {this.state.status === this.APP_STATE.LOADING ? (
          <div className="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        ) : undefined}
        {this.state.status === this.APP_STATE.ERROR ? (
          <div className="error-message">An error occured </div>
        ) : undefined}
        {this.state.status === this.APP_STATE.LOADED ? (
          <>
            <div className="newslist-container">
              <ul>
                {filteredNewsArray.map((news) => {
                  return <News newsDetails={news} />;
                })}
              </ul>
            </div>
          </>
        ) : undefined}
      </>
    );
  }
}

export default NewsLists;
