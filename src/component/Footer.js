import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <>
        <div id="footer">
          <ul id="footer-list">
            <li>About</li>
            <li>•</li>
            <li>Setting</li>
            <li>•</li>
            <li>Help</li>
            <li>•</li>
            <li>API Documentation</li>
            <li>•</li>
            <li>Hacker news</li>
            <li>•</li>
            <li>Fork/Contribute</li>
            <li>•</li>
            <li>Cool Apps</li>
          </ul>
        </div>
      </>
    );
  }
}
