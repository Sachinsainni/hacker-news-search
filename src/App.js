import './App.css';
import Footer from './component/Footer';
// import Header from './component/Header';
import NewsLists from './component/NewsLists';


function App() {
  return (
    <>
    {/* <Header/> */}
    <NewsLists/>
    <Footer/>
    </>
  );
}

export default App;
